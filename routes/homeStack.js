import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import LoginScreen from '../screen/login';
import Location from '../screen/location';

const screens = {
    Login : {
        screen: LoginScreen
    },
    Location: {
        screen: Location
    },
}


const HomeStack = createStackNavigator(screens);

export default createAppContainer(HomeStack);
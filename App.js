import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import LoginScreen from './screen/login';
import Location from './screen/location';
import Navigator from './routes/homeStack';



export default class App extends React.Component {
  render() {
    const { navigation } = this.props;
    return (
      <Navigator />
  
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
